import pyrebase
import firebase_admin
import sys
import numpy as np
from firebase_admin import credentials
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class Database():
    def __init__(self):
        self.config = {
            "apiKey": "AIzaSyDHq3pmxg6mthhjF40fun5ryEYRD5ccisM",
             "authDomain": "pythonproj1995.firebaseapp.com",
             "databaseURL": "https://pythonproj1995.firebaseio.com",
             "storageBucket": "pythonproj1995.appspot.com",
             "serviceAccount": "./pythonproj1995-0feec455793a.json"
            }
        self.cred = credentials.Certificate("./pythonproj1995-firebase-adminsdk-h76s2-6495e761cd.json")
        self.firebase = pyrebase.initialize_app(self.config)
        self.db = self.firebase.database()
        self.auth = self.firebase.auth()
        self.user = {}

    def newSignIn(self, email, password):
        self.user = self.auth.sign_in_with_email_and_password(email, password)
        return self.user

    def createUser(self, email, password):
        self.auth.create_user_with_email_and_password(email, password)

    def newBorrowedItem(self ,owner, item):
        item = {"owner": owner, "item": item}
        uid = self.user['localId']
        self.db.child("users").child(uid).child("borrowed").child(item).set(item, self.user['idToken'])

    def newLentItem(self ,name, item):
        item = {"borrower": name, "item": item}
        uid = self.user['localId']
        self.db.child("users").child(uid).child("lent").child(item).set(item, self.user['idToken'])

    def readBorrowedItems(self):
        uid = self.user['localId']
        all_borrowed = self.db.child("users").child(uid).child("borrowed").get(self.user['idToken']).val()
        return all_borrowed

    def readLentItems(self):
        uid = self.user['localId']
        all_lent = self.db.child("users").child(uid).child("lent").get(self.user['idToken']).val()
        return all_lent

    def deleteBorrowedItem(self, name, item1):
        item = {"owner": name, "item": item1}
        uid = self.user['localId']
        self.db.child("users").child(uid).child("borrowed").child(item).remove(self.user['idToken'])

    def deleteLentItem(self, name, item1):
        item = {"borrower": name, "item": item1}
        uid = self.user['localId']
        self.db.child("users").child(uid).child("lent").child(item).remove(self.user['idToken'])


class LoginWindow(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall1.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        Gbox = QGridLayout()
        self.label = QLabel('Welcome to Moocher!', self)
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        self.label.setAlignment(Qt.AlignCenter)
        self.newUserButton = QPushButton('Create New User', self)
        self.newUserButton.clicked.connect(self.newUserWind)
        self.signInUser = QPushButton('Sign In', self)
        self.signInUser.clicked.connect(self.signIn)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        Gbox.addWidget(self.label)
        Gbox.addWidget(self.newUserButton)
        Gbox.addWidget(self.signInUser)
        self.setLayout(Gbox)
        self.theDB = Database()
        self.show()

    def newUserWind(self):
        self.hide()
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall2.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.label = QLabel("New User")
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        Fbox = QFormLayout()
        Fbox.setContentsMargins(65, 0, 0, 70)
        e1 = QLineEdit()
        e2 = QLineEdit()
        e2.setEchoMode(QLineEdit.Password)
        Fbox.addRow(" ", e1)
        Fbox.addRow(" ", e1)
        self.label.setAlignment(Qt.AlignTop)
        Fbox.addWidget(self.label)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        Fbox.addItem(verticalSpacer)
        Fbox.addRow("Email", e1)
        Fbox.addRow("Password", e2)
        submitUser = QPushButton("Submit")
        submitUser.setStyleSheet("*{margin-right:54px; padding:4px;}")
        submitUser.clicked.connect(lambda: self.makeUser(e1, e2))
        Fbox.addRow(submitUser)
        self.setLayout(Fbox)
        self.show()
        self.check = True

    def signIn(self):
        self.hide()
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall2.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.label = QLabel('Sign In')
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        Fbox = QFormLayout()
        Fbox.setContentsMargins(70, 0, 0, 70)
        e1 = QLineEdit()
        e2 = QLineEdit()
        e2.setEchoMode(QLineEdit.Password)
        Fbox.addRow(" ", e1)
        Fbox.addRow(" ", e1)
        self.label.setAlignment(Qt.AlignTop)
        Fbox.addWidget(self.label)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        Fbox.addItem(verticalSpacer)
        Fbox.addRow("Email:", e1)
        Fbox.addRow("Password", e2)
        newSignIn = QPushButton("Submit")
        newSignIn.setStyleSheet("*{margin-right:54px; padding:4px;}")
        newSignIn.clicked.connect(lambda: self.postSignInClick(e1, e2))
        Fbox.addRow(newSignIn)
        self.setLayout(Fbox)
        self.show()

    def postSignInClick(self, e1, e2):
        email = e1.text()
        password = e2.text()
        try:
            self.theDB.newSignIn(email, password)
        except:
            QMessageBox.warning(self, 'Error', 'Invalid email or password')
        else:
            self.hide()
            self.enterWindow(e1)


    def makeUser(self, e1, e2):
        email = e1.text()
        password = e2.text()
        try:
            self.theDB.createUser(email, password)
        except:
            QMessageBox.warning(self, 'Error', 'Cant use these credentials')
        else:
            self.hide()
            self.theDB.newSignIn(email, password)
            self.enterWindow(e1)

    def enterWindow(self, e1):
        email = e1.text()
        self.hide()
        Borrowed = self.theDB.readBorrowedItems()
        Lent = self.theDB.readLentItems()
        QWidget.__init__(self)
        self.setGeometry(400, 620, 620, 400)
        backImage = QImage("./assets/wall3.jpg")
        sImage = backImage.scaled(QSize(620, 400))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.FboxU = QFormLayout()
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.FboxU.addItem(verticalSpacer)
        label1 = QLabel("Borrowed Items          ")
        label2 = QLabel("               Lent Items")
        label1.setStyleSheet("*{color:black; font-size:24px;}")
        label2.setStyleSheet("*{color:black; font-size:24px;}")
        self.listWidgetB = QListWidget()
        self.listWidgetL = QListWidget()
        self.FboxU.addRow(label1, label2)
        self.FboxU.addItem(verticalSpacer)
        if Borrowed:
            for i in Borrowed.values():
                self.listWidgetB.addItem("Item: " + i['item'] + " | Borrowed by: " + i['owner'])
        if Lent:
            for a in Lent.values():
                self.listWidgetL.addItem("Item: " + a['item'] + " | Owned by: " + a['borrower'])
        self.FboxU.addRow(self.listWidgetB, self.listWidgetL)
        newBorrowedItem = QPushButton("Add New Borrowed Item")
        newLentItem = QPushButton("Add New Lent Item")
        deleteBorrowedItem = QPushButton("Delete Borrowed Item")
        deleteLentItem = QPushButton("Delete Lent Item")
        newBorrowedItem.clicked.connect(lambda: self.addNewBorrowedItem())
        newLentItem.clicked.connect(lambda: self.addNewLentItem())
        deleteBorrowedItem.clicked.connect(lambda: self.deleteNewBorrowedItem())
        deleteLentItem.clicked.connect(lambda: self.deleteNewLentItem())
        self.FboxU.addRow(newBorrowedItem, newLentItem)
        self.FboxU.addRow(deleteBorrowedItem, deleteLentItem)
        self.setLayout(self.FboxU)
        self.setWindowTitle('Hello ' + email +'! Here are your Items')
        self.show()

    def addNewBorrowedItem(self):
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall2.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.label = QLabel("Add Item")
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        Fbox = QFormLayout()
        Fbox.setContentsMargins(70, 0, 0, 70)
        e1 = QLineEdit()
        e2 = QLineEdit()
        Fbox.addRow(" ", e1)
        Fbox.addRow(" ", e1)
        self.label.setAlignment(Qt.AlignTop)
        Fbox.addWidget(self.label)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        Fbox.addItem(verticalSpacer)
        Fbox.addRow("  Owner", e1)
        Fbox.addRow("  Item", e2)
        newBorrowedItem1 = QPushButton("Submit")
        newBorrowedItem1.clicked.connect(lambda: self.BitemToDB(e1, e2))
        newBorrowedItem1.setStyleSheet("*{margin-right:54px; padding:4px;}")
        Fbox.addRow(newBorrowedItem1)
        self.setLayout(Fbox)
        self.show()

    def addNewLentItem(self):
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall2.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.label = QLabel("Add Item")
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        Fbox = QFormLayout()
        Fbox.setContentsMargins(70, 0, 0, 70)
        e1 = QLineEdit()
        e2 = QLineEdit()
        Fbox.addRow(" ", e1)
        Fbox.addRow(" ", e1)
        self.label.setAlignment(Qt.AlignTop)
        Fbox.addWidget(self.label)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        Fbox.addItem(verticalSpacer)
        Fbox.addRow(" Borrower", e1)
        Fbox.addRow(" Item", e2)
        newLentItem1 = QPushButton("Submit")
        newLentItem1.setStyleSheet("*{margin-right:54px; padding:4px;}")
        newLentItem1.clicked.connect(lambda: self.LitemToDB(e1, e2))
        Fbox.addRow(newLentItem1)
        self.setLayout(Fbox)
        self.setWindowTitle('New Lent Item')
        self.show()

    def deleteNewBorrowedItem(self):
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall2.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.label = QLabel("Delete Item    ")
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        Fbox = QFormLayout()
        Fbox.setContentsMargins(40, 0, 0, 40)
        e1 = QLineEdit()
        e2 = QLineEdit()
        Fbox.addRow(" ", e1)
        Fbox.addRow(" ", e1)
        self.label.setAlignment(Qt.AlignTop)
        Fbox.addWidget(self.label)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        Fbox.addItem(verticalSpacer)
        Fbox.addRow("             Owner", e1)
        Fbox.addRow("             Item", e2)
        deleteBorrowedItem1 = QPushButton("Submit")
        deleteBorrowedItem1.setStyleSheet("*{margin-right:54px; margin-left:22px; padding:4px;}")
        deleteBorrowedItem1.clicked.connect(lambda: self.BitemLeaveDB(e1, e2))
        Fbox.addRow(deleteBorrowedItem1)
        self.setLayout(Fbox)
        self.setWindowTitle('Delete Borrowed Item')
        self.show()

    def deleteNewLentItem(self):
        QWidget.__init__(self)
        self.setGeometry(350, 350, 350, 350)
        backImage = QImage("./assets/wall2.jpg")
        sImage = backImage.scaled(QSize(350, 350))
        palette = QPalette()
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)
        self.label = QLabel("Delete Item    ")
        self.label.setStyleSheet("*{color:black; font-size:24px;}")
        Fbox = QFormLayout()
        Fbox.setContentsMargins(40, 0, 0, 40)
        e1 = QLineEdit()
        e2 = QLineEdit()
        Fbox.addRow(" ", e1)
        Fbox.addRow(" ", e1)
        self.label.setAlignment(Qt.AlignTop)
        Fbox.addWidget(self.label)
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        Fbox.addItem(verticalSpacer)
        Fbox.addRow("             Borrower", e1)
        Fbox.addRow("             Item", e2)
        deleteLentItem1 = QPushButton("Submit")
        deleteLentItem1.setStyleSheet("*{margin-right:54px; margin-left:22px; padding:4px;}")
        deleteLentItem1.clicked.connect(lambda: self.LitemLeaveDB(e1, e2))
        Fbox.addRow(deleteLentItem1)
        self.setLayout(Fbox)
        self.setWindowTitle('Delete Lent Item')
        self.show()

    def BitemToDB(self, e1, e2):
        Name = e1.text()
        Item = e2.text()
        self.theDB.newBorrowedItem(Name, Item)
        newBorrowed = self.theDB.readBorrowedItems()
        self.listWidgetB.clear()
        for a in newBorrowed.values():
            self.listWidgetB.addItem("Item: " + a['item'] + " | Owned by: " + a['owner'])
        self.show()

    def BitemLeaveDB(self, e1, e2):
        Name = e1.text()
        Item = e2.text()
        self.theDB.deleteBorrowedItem(Name, Item)
        newBorrowed = self.theDB.readBorrowedItems()
        self.listWidgetB.clear()
        for a in newBorrowed.values():
            self.listWidgetB.addItem("Item: " + a['item'] + " | Owned by: " + a['owner'])
        self.show()

    def LitemToDB(self, e1, e2):
        Name = e1.text()
        Item = e2.text()
        self.theDB.newLentItem(Name, Item)
        newLent = self.theDB.readLentItems()
        self.listWidgetL.clear()
        for a in newLent.values():
            self.listWidgetL.addItem("Item: " + a['item'] + " | Owned by: " + a['borrower'])
        self.show()

    def LitemLeaveDB(self, e1, e2):
        Name = e1.text()
        Item = e2.text()
        self.theDB.deleteLentItem(Name, Item)
        newLent = self.theDB.readLentItems()
        self.listWidgetL.clear()
        for a in newLent.values():
            self.listWidgetL.addItem("Item: " + a['item'] + " | Owned by: " + a['borrower'])
        self.show()



if __name__ == "__main__":
    app = QApplication(sys.argv)
    newWind = LoginWindow()
    sys.exit(app.exec_())

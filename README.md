# Python Project - Moocher

##Group Members: Tyler Velazquez, Evan Schwalb and Abigail Arreguin

##Description of Project
Project Title: Moocher  
Moocher is a web application capable of tracking all of the borrowed items that you have lent to others and all of the items friends have lent to you. It is going to have a simple UI, consisting of the list of things that you have lent out or borrowed. 

##Technologies Used
The technologies we use include slack for open communication, trello for organizing tasks and bitbucket for source code integration.

##Libraries Used
PyQT5 - pip3 install pyqt5
FireBase - pip3 install pyrebase

##Seperation of Work
Evan worked mostly on the backend including database and storage.
Abby worked on the UI and layout. 
Tyler worked on tying the UI with the database and helped with bugs and functionality.

##Deployment
`python3 moocher.py`

##Known Bugs
There are no known bugs.